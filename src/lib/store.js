import { configureStore } from '@reduxjs/toolkit'
import ProblemReducer from "@/lib/features/problem/problemSlice";

export default configureStore({
    reducer: {
        problem : ProblemReducer
    },
})
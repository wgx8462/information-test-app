import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    problems: [
        {ai: '과학', prob: '물의 특성을 설명하시오.'},
        {ai: '요리', prob: '김치볶음밥 만드는 방법 서술하시오.'},
        {ai: '수학', prob: '10 - 10 * 10 + 10 의 풀이 순서를 서술하시오.'},
        {ai: '운동', prob: '웨이트 트레이닝의 3대 운동은 무엇인가?'},
        {ai: '상식', prob: '대한민국의 국화는 무엇인가요?'},
        {ai: '미스테리', prob: '버뮤다 삼각지대"라고 불리는 지역은 대서양의 어느 부분에 위치해 있는가?'},
        {ai: '경제', prob: '인공지능 기술 발전이 미래 경제에 미칠 영향을 예측하고, 이에 대비하기 위한 전략을 설명하시오.'},
        {ai: 'e스포츠', prob: '게임을 잘하기 위해서는 무엇이 필요할까?'},
        {ai: '과학', prob: '인간의 유전 정보는 몇 개의 염색체에 담겨 있는가?'},
        {ai: '요리', prob: '고기를 낮은 온도에서 오랫동안 천천히 익히는 조리 방법을 무엇이라 하는가?'},
        {ai: '수학', prob: '삼각형의 세 변의 길이가 각각 7, 24, 25일 때, 이 삼각형의 넓이는 얼마인가?'},
        {ai: '운동', prob: '웨이트 트레이닝 중에 근육이 성장하는 과정에서 일어나는 현상은 무엇인가?'},
        {ai: '상식', prob: '최초의 스마트폰은 어느 회사에서 개발했는가?'},
        {ai: '미스테리', prob: ' 2003년, 미국의 워싱턴 D.C.에서 박사과정 학생이었던 레이몬드 카사의 죽음을 둘러싼 미스터리 사건은 무엇인가?'},
        {ai: '경제', prob: '독점 시장에서 기업의 가격 결정력은 어떻게 되는가?'},
        {ai: 'e스포츠', prob: '최초의 e스포츠 대회 중 하나로, 1997년에 열린 대회는 무엇인가?'},
    ],
    pickProblem: [],
    answer: ''
}

const ProblemSlice = createSlice({
    name: 'problem',
    initialState,
    reducers: {
        reset: (state) => {
            state.answer = ''
            state.pickProblem = []
        },
        pickProble: (state) => {
            const randomIndex = Math.floor(Math.random() * state.problems.length)
            state.pickProblem = state.problems[randomIndex]
        },
        setAnswer: (state, action) => {
            state.answer = action.payload
        }
    }
})

export const {pickProble, reset, setAnswer} = ProblemSlice.actions;
export default ProblemSlice.reducer
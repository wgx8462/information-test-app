'use client'

import { useSelector } from "react-redux";
import { useRouter } from "next/navigation";

export default function Devote() {
    const {pickProblem} = useSelector(state => state.problem);
    const router = useRouter();

    const handleNextPage = () => {
        router.push("/devote2");
    }

    return (
        <div onClick={handleNextPage} className='flex flex-col items-center justify-between backImg mt-32 p-72'>
            <div className='text-4xl bg-amber-50 border-4 border-amber-50 rounded-lg -mt-44'>주제는 {pickProblem.ai}</div>
        </div>

    )
}
'use client'

import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/navigation";
import {reset} from "@/lib/features/problem/problemSlice";

export default function Result() {
    const { answer } = useSelector(state => state.problem);
    const router = useRouter();
    const dispatch = useDispatch();

    const handleReset = () => {
        dispatch(reset())
        router.push("/");
    }

    return (
        <div className='flex  flex-col items-center justify-between p-24'>
            <div className='text-base text-width'>
                {
                    answer.split('다.').map((el, indx, arr) => {
                        if (el.trim() === '' && indx === arr.length - 1) return null;  // 마지막 요소가 빈 문자열이면 아무것도 반환하지 않음
                        return <span key={indx}>{el.trim()}{indx < arr.length - 1 ? '다.' : ''}<br/><br/></span>;
                    })
                }
            </div>

            <button type="button" onClick={handleReset}
                    className="text-white bg-gray-800 hover:bg-gray-900 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 me-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700">다시하기
            </button>
        </div>
    )
}
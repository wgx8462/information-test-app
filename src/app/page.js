'use client'

import Image from "next/image";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import {pickProble} from "@/lib/features/problem/problemSlice";

export default function Home() {
    const router = useRouter();
    const dispatch = useDispatch();

    const handlePageChange = () => {
        dispatch(pickProble())
        router.push("/devote");
    }

    return (
        <main className="flex  flex-col items-center justify-between p-24">
            <div>
                <h1 className='text-5xl font-bold p-24'>너의 <br/>지식이 보여!</h1>
            </div>
            <button onClick={handlePageChange}
                className='text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>시작하기
            </button>
        </main>
    );
}

'use client'

import {useState} from "react";
import axios from "axios"
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/navigation";
import {setAnswer} from "@/lib/features/problem/problemSlice";

export default function Devote2() {
    const {pickProblem} = useSelector(state => state.problem);
    const [response, setResponse] = useState('');
    const [devoted, setDevoted] = useState('')
    const [loading, setLoading] = useState(false)
    const dispatch = useDispatch();
    const router = useRouter();

    const handleConfirm = () => {
        setLoading(true)
        const means = pickProblem.prob
        const commend = `유저는 문제에 대한 답변을 할건데 너는 그 답변을 세세하게 평가해서 점수로 환산해야해. 꼭 100점 만점 기준으로 환산해서 맨 처음에 점수부터 알려주고 시작해 문제 : ${means}`
        const commend2 = devoted
        const apiUrl = 'https://generativelanguage.googleapis.com/v1beta/models/gemini-1.5-flash:generateContent?key=AIzaSyBIyFKDg5lSdJWsIR3nR6-0AJmFv39TI7U'

        const data = {
            "contents": [{"role": "user", "parts": [{"text": commend}]}, {
                "role": "user",
                "parts": [{"text": commend2}]
            }]
        }
        axios.post(apiUrl, data)
            .then(res => {
                const responseData = res.data.candidates[0].content.parts[0].text.replaceAll('*', '').replaceAll('#', '');
                setResponse(responseData)
                dispatch(setAnswer(responseData)) // 데이터가 설정된 후 dispatch 실행
                router.push("/result")
            })
            .catch(err => {
                console.log(err)
                setLoading(false)
            })
    }

    return (
        <div className='flex  flex-col items-center justify-between p-40'>
            {loading ? (
                <button disabled type="button"
                        className="text-black bg-white hover:bg-white focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-lg px-6 py-3 text-center me-2 dark:bg-white dark:hover:bg-white dark:focus:ring-gray-400 inline-flex items-center">
                    <svg aria-hidden="true" role="status" className="inline w-5 h-5 me-3 text-black animate-spin"
                         viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="#E5E7EB"/>
                        <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentColor"/>
                    </svg>
                    Loading...
                </button>


            ) : (
                <>
                    <div className='text-2xl mt-7'>
                        {pickProblem.prob}
                    </div>
                    <div className="mb-6 ">
                        <textarea id="message" rows="4" value={devoted} onChange={e => setDevoted(e.target.value)}
                                  className="mt-10 block p-2.5 max-w-full text-sm text-gray-600 bg-blue-100 rounded-lg border border-blue-200 focus:ring-teal-400 focus:border-teal-400 dark:bg-blue-900 dark:border-blue-800 dark:placeholder-blue-300 dark:text-gray-200 dark:focus:ring-teal-400 dark:focus:border-teal-400"
                                  placeholder="문제의 답을 적어서 제출하세요.">
                        </textarea>
                    </div>
                    <button type="button" onClick={handleConfirm}
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-blue-800 dark:hover:bg-blue-700 dark:focus:ring-blue-800">제출
                    </button>
                </>
            )}
        </div>
    )
}